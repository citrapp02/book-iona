package com.bookiona.bookionacitra.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderDetailsKey implements Serializable{
	
	@Column(name = "book_id")
	private long bookID;
	
	@Column(name = "order_id")
	private long orderID;

	public OrderDetailsKey() {
		
	}
	
	public OrderDetailsKey(long bookID, long orderID) {
		super();
		this.bookID = bookID;
		this.orderID = orderID;
	}

	public long getBookID() {
		return bookID;
	}

	public void setBookID(long bookID) {
		this.bookID = bookID;
	}

	public long getOrderID() {
		return orderID;
	}

	public void setOrderID(long orderID) {
		this.orderID = orderID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (bookID ^ (bookID >>> 32));
		result = prime * result + (int) (orderID ^ (orderID >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderDetailsKey other = (OrderDetailsKey) obj;
		if (bookID != other.bookID)
			return false;
		if (orderID != other.orderID)
			return false;
		return true;
	}


	
	

}

