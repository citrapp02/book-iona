package com.bookiona.bookionacitra.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class OrderDetails implements Serializable {

	@EmbeddedId
	OrderDetailsKey id;
	
	@ManyToOne
	@MapsId("book_id")
	@JoinColumn(name = "book_id")
	private Book book;
	
	@ManyToOne
	@MapsId("order_id")
	@JoinColumn(name = "order_id")
	private Orders order;
	
	private int quantity;	
	private BigDecimal tax;
	private BigDecimal discount;
	
	public OrderDetails() {
		
	}
	
	public OrderDetails(OrderDetailsKey id, Book book, Orders order, int quantity, BigDecimal tax,
			BigDecimal discount) {
		super();
		this.id = id;
		this.book = book;
		this.order = order;
		this.quantity = quantity;
		this.tax = tax;
		this.discount = discount;
	}

	public OrderDetailsKey getId() {
		return id;
	}

	public void setId(OrderDetailsKey id) {
		this.id = id;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Orders getOrder() {
		return order;
	}

	public void setOrder(Orders order) {
		this.order = order;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	
}
