package com.bookiona.bookionacitra.models;

import java.util.Set;

import javax.persistence.*;

@Entity
public class Publisher {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long publisherID;
	
	@Column(nullable = false)
	private String companyName;
	
	@Column(nullable = false)
	private String country;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "paperID", nullable = false)
	private Paper paper;
	
	@OneToMany(mappedBy = "publisher")
	private Set<Book> books;
	
	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

	public Publisher() {
    }

	public long getPublisherID() {
		return publisherID;
	}

	public void setPublisherID(long publisherID) {
		this.publisherID = publisherID;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Paper getPaper() {
		return paper;
	}

	public void setPaper(Paper paper) {
		this.paper = paper;
		paper.getPublishers().add(this);
	}
	
}

