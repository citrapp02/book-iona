package com.bookiona.bookionacitra.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Reviewers{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long reviewerID;
	
	@Column(nullable = false)
	private String reviewerName;
	
	@Column(nullable = false)
	private String country;
	
	@Column(nullable = false)
	private Boolean verified;
	
	@OneToMany(mappedBy = "reviewer")
	Set<BookRatings> rating;
	
	public Reviewers() {
		
	}

	public Reviewers(long reviewerID, String reviewerName, String country, Boolean verified) {
		super();
		this.reviewerID = reviewerID;
		this.reviewerName = reviewerName;
		this.country = country;
		this.verified = verified;
	}

	public long getReviewerID() {
		return reviewerID;
	}

	public void setReviewerID(long reviewerID) {
		this.reviewerID = reviewerID;
	}

	public String getReviewerName() {
		return reviewerName;
	}

	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}
	
	
	
}

