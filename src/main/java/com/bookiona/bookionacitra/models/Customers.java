package com.bookiona.bookionacitra.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Customers implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long customersID;
	
	@Column(nullable = false)
	private String customerName;
	
	@Column(nullable = false)
	private String country;
	
	@Column(nullable = false)
	private String address;
	
	@Column(nullable = false)
	private String phoneNumber;
	
	@Column(nullable = false)
	private String postalCode;
	
	@Column(nullable = false)
	private String email;
	
	@OneToMany(mappedBy = "customers")
	private Set<Orders> orders;
	
	public Customers(long customersID, String customerName, String country, String address, String phoneNumber,
			String postalCode, String email, Set<Orders> orders) {
		super();
		this.customersID = customersID;
		this.customerName = customerName;
		this.country = country;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.postalCode = postalCode;
		this.email = email;
		this.orders = orders;
	}

	public Customers() {
		
	}

	public Customers(long customersID, String customerName, String country, String address, String phoneNumber,
			String postalCode, String email) {
		super();
		this.customersID = customersID;
		this.customerName = customerName;
		this.country = country;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.postalCode = postalCode;
		this.email = email;
	}

	public long getCustomersID() {
		return customersID;
	}

	public void setCustomersID(long customersID) {
		this.customersID = customersID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Orders> getOrders() {
		return orders;
	}

	public void setOrders(Set<Orders> orders) {
		this.orders = orders;
	}
	
	
}

