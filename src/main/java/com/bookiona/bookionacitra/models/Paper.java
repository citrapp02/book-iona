package com.bookiona.bookionacitra.models;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name = "paper")
public class Paper {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long paperID;
	
	@Column(nullable = false)
	private String qualityName;
	
	@Column(nullable = false)
	private BigDecimal paperPrice;
	
	@OneToMany(mappedBy = "paper")
	private Set<Publisher> publishers;
	 
	public Paper() {
		publishers = new HashSet<>();
	}
	
	public Paper(String name) {
        this.qualityName = name;
        publishers = new HashSet<>();
    }

	public long getPaperID() {
		return paperID;
	}

	public void setPaperID(long paperID) {
		this.paperID = paperID;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public BigDecimal getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}

	public Set<Publisher> getPublishers() {
		return publishers;
	}

	public void setPublisher(Set<Publisher> publishers) {
        this.publishers = publishers;
        for (Publisher publisher : publishers) {
        	publisher.setPaper(this);
        }
    }
	
}
