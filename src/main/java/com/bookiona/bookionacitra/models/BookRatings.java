package com.bookiona.bookionacitra.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class BookRatings implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ratingID;
	
	@ManyToOne
	@JoinColumn(name = "book_id")
	private Book book;
	
	@ManyToOne
	@JoinColumn(name = "reviewer_id")
	private Reviewers reviewer;
	
	private int ratingScore;

	public BookRatings() {
		
	}
	
	public BookRatings(long ratingID, Book book, Reviewers reviewer) {
		super();
		this.ratingID = ratingID;
		this.book = book;
		this.reviewer = reviewer;
	}

	public long getRatingID() {
		return ratingID;
	}

	public void setRatingID(long ratingID) {
		this.ratingID = ratingID;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Reviewers getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewers reviewer) {
		this.reviewer = reviewer;
	}

	public int getRatingScore() {
		return ratingScore;
	}

	public void setRatingScore(int ratingScore) {
		this.ratingScore = ratingScore;
	}
	
	
}
