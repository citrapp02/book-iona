package com.bookiona.bookionacitra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookIonaCitraApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookIonaCitraApplication.class, args);
	}

}
