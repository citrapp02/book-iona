package com.bookiona.bookionacitra.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bookiona.bookionacitra.models.BookRatings;

@Repository
public interface BookRatingRepository extends JpaRepository<BookRatings, Long> {

}
