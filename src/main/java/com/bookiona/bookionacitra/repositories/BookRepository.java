package com.bookiona.bookionacitra.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bookiona.bookionacitra.models.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
