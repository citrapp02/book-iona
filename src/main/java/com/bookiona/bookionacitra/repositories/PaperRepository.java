package com.bookiona.bookionacitra.repositories;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bookiona.bookionacitra.models.Paper;

@Repository
public interface PaperRepository extends JpaRepository<Paper, Long> {

}
