package com.bookiona.bookionacitra.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bookiona.bookionacitra.models.Reviewers;

@Repository
public interface ReviewerRepository extends JpaRepository<Reviewers, Long> {

}
