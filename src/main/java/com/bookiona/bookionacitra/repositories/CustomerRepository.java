package com.bookiona.bookionacitra.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bookiona.bookionacitra.models.Customers;

@Repository
public interface CustomerRepository extends JpaRepository<Customers, Long> {

}
