package com.bookiona.bookionacitra.ols.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookiona.bookionacitra.models.Author;
import com.bookiona.bookionacitra.ols.AuthorOLO;
import com.io.iona.springboot.controllers.HibernateOptionListController;

@RestController
@RequestMapping("/ol/author")
public class AuthorOLOController extends HibernateOptionListController<Author, AuthorOLO> {

}
