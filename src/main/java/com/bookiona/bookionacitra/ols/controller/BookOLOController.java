package com.bookiona.bookionacitra.ols.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookiona.bookionacitra.models.Book;
import com.bookiona.bookionacitra.ols.BookOLO;
import com.io.iona.springboot.controllers.HibernateOptionListController;

@RestController
@RequestMapping("/ol/book")
public class BookOLOController extends HibernateOptionListController<Book, BookOLO> {

}
