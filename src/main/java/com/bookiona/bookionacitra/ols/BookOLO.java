package com.bookiona.bookionacitra.ols;

import com.io.iona.core.data.annotations.OptionListKey;

public class BookOLO {
	@OptionListKey
	private long bookID;
	private String title;

	public long getBookID() {
		return bookID;
	}

	public void setBookID(long bookID) {
		this.bookID = bookID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
