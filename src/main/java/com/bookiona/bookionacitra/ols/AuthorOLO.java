package com.bookiona.bookionacitra.ols;

import com.io.iona.core.data.annotations.OptionListKey;

public class AuthorOLO {
	@OptionListKey
	private long authorID;
	private String firstName;

	public long getAuthorID() {
		return authorID;
	}

	public void setAuthorID(long authorID) {
		this.authorID = authorID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

}
