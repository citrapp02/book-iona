package com.bookiona.bookionacitra.view.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookiona.bookionacitra.view.dtos.VmBookInfoDTO;
import com.bookiona.bookionacitra.views.VmBookInfo;
import com.io.iona.springboot.controllers.HibernateViewController;

@RestController
@RequestMapping("/api/viewbookinfo")
public class VmBookInfoController extends HibernateViewController<VmBookInfo, VmBookInfoDTO>{

}
