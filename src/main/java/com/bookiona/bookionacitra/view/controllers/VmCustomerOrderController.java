package com.bookiona.bookionacitra.view.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookiona.bookionacitra.view.dtos.VmCustomerOrderDTO;
import com.bookiona.bookionacitra.views.VmCustomerOrder;
import com.io.iona.springboot.controllers.HibernateViewController;

@RestController
@RequestMapping("/api/viewcustomerorder")
public class VmCustomerOrderController extends HibernateViewController<VmCustomerOrder, VmCustomerOrderDTO> {

}
