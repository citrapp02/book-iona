package com.bookiona.bookionacitra.view.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookiona.bookionacitra.view.dtos.VmBookReviewerDTO;
import com.bookiona.bookionacitra.views.VmBookReviewer;
import com.io.iona.springboot.controllers.HibernateViewController;

@RestController
@RequestMapping("/api/viewbookreviewer")
public class VmBookReviewerController extends HibernateViewController<VmBookReviewer, VmBookReviewerDTO> {

}
